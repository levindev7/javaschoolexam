package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        System.out.println(statement);
        if (statement == null) {
            return null;
        }
        while (true) {
            if (statement.contains("(")) {
                try {
                    String subStatement = statement.substring(statement.indexOf("(") + 1, statement.indexOf(")"));
                    if (subStatement.contains("(") || subStatement.contains(")")) {
                        return null;
                    }
                    subStatement = calcDouble(subStatement);
                    statement = statement.substring(0, statement.indexOf("(")) + subStatement + statement.substring(statement.indexOf(")") + 1);
                }
                catch (Exception ex) {
                    return null;
                }
            }
            else break;
        }
        statement = calcDouble(statement);
        return statement;
    }


    private static String calcDouble (String statementCalc) {
        String roundResult;
        double DivMulti;
        double sumSub;
        double afterDot;
        try {
            if (statementCalc.contains("/-") || statementCalc.contains("*-")) {
                String[] operatorsSumSub = statementCalc.split("[.*/0-9]+");
                String[] searchDivisionMulti = statementCalc.split("[+]");
                String x;
                for (int count = 0; count < searchDivisionMulti.length; count++) {
                    x = searchDivisionMulti[count];
                    if (x.contains("/") || x.contains("*")) {

                        String[] operatorsDivisionMulti = x.split("[-.0-9]+");
                        String[] operandsDivMulti = x.split("[*/]");

                        DivMulti = Double.parseDouble(operandsDivMulti[0]);

                        for (int i = 1; i < operandsDivMulti.length; i++) {
                            if (operatorsDivisionMulti[i].equals("*")) {
                                DivMulti *= Double.parseDouble(operandsDivMulti[i]);
                            }
                            if (operatorsDivisionMulti[i].equals("/")) {
                                if (Double.parseDouble(operandsDivMulti[i]) == 0) {
                                    return null;
                                }
                                DivMulti /= Double.parseDouble(operandsDivMulti[i]);
                            }
                        }
                        x = String.valueOf(DivMulti);
                    }
                    searchDivisionMulti[count] = x;
                }

                sumSub = Double.parseDouble(searchDivisionMulti[0]);
                for (int i = 1; i < searchDivisionMulti.length; i++) {
                    if (operatorsSumSub[i].equals("+")) {
                        sumSub += Double.parseDouble(searchDivisionMulti[i]);
                    }
                    if (operatorsSumSub[i].equals("-")) {
                        sumSub -= Double.parseDouble(searchDivisionMulti[i]);
                    }
                }
                int integerValue = (int) sumSub;
                afterDot = sumSub - integerValue;
                if (afterDot == 0) {
                    statementCalc = statementCalc.replace(statementCalc, String.valueOf(integerValue));
                }
                else {
                    NumberFormat nf = NumberFormat.getNumberInstance(Locale.UK);
                    DecimalFormat df4 = (DecimalFormat) nf;
                    df4.applyPattern("#.####");
                    roundResult = df4.format(sumSub);
                    statementCalc = statementCalc.replace(statementCalc, roundResult);
                }
            } else {
                String[] operatorsSumSub = statementCalc.split("[.*/0-9]+");
                String[] searchDivisionMulti = statementCalc.split("[-+]");
                String x = "";
                for (int count = 0; count < searchDivisionMulti.length; count++) {
                    x = searchDivisionMulti[count];
                    if (x.contains("/") || x.contains("*")) {
                        String[] operatorsDivisionMulti = x.split("[.0-9]+");
                        String[] operandsDivMulti = x.split("[*/]");
                        DivMulti = Double.parseDouble(operandsDivMulti[0]);

                        for (int i = 1; i < operandsDivMulti.length; i++) {
                            if (operatorsDivisionMulti[i].equals("*")) {
                                DivMulti *= Double.parseDouble(operandsDivMulti[i]);
                            }
                            if (operatorsDivisionMulti[i].equals("/")) {
                                if (Double.parseDouble(operandsDivMulti[i]) == 0) {
                                    return null;
                                }
                                DivMulti /= Double.parseDouble(operandsDivMulti[i]);
                            }
                        }
                        x = String.valueOf(DivMulti);
                    }
                    searchDivisionMulti[count] = x;
                }
                sumSub = Double.parseDouble(searchDivisionMulti[0]);
                for (int i = 1; i < searchDivisionMulti.length; i++) {
                    if (operatorsSumSub[i].equals("+")) {
                        sumSub += Double.parseDouble(searchDivisionMulti[i]);
                    }
                    if (operatorsSumSub[i].equals("-")) {
                        sumSub -= Double.parseDouble(searchDivisionMulti[i]);
                    }
                }
                int integerValue = (int) sumSub;
                afterDot = sumSub - integerValue;
                if (afterDot == 0) {
                    statementCalc = statementCalc.replace(statementCalc, String.valueOf(integerValue));
                } else {
                    NumberFormat nf = NumberFormat.getNumberInstance(Locale.UK);
                    DecimalFormat df4 = (DecimalFormat) nf;
                    df4.applyPattern("#.####");
                    roundResult = df4.format(sumSub);
                    statementCalc = statementCalc.replace(statementCalc, roundResult);
                }
            }
        }
        catch (Exception e) {
            return null;
        }
        return statementCalc;
    }
}
