package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        if (inputNumbers.size() > Integer.MAX_VALUE - 2) throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);

        int columns = 0;
        int rows = 0;
        int i = 0;
        int pyramidSize = 0;

        while (pyramidSize < inputNumbers.size()) {
            i++;
            pyramidSize += i;
            if (inputNumbers.size() == pyramidSize) {
                columns = i;
                rows = (i * 2) - 1;
            }
            if (inputNumbers.size() % pyramidSize == inputNumbers.size()) throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[columns][rows];

        int offset = columns;
        int countNumber = 0;
        for (int countColumns = 0; countColumns < columns; countColumns++) {
            offset--;
            int setNumber = columns - 1 - countColumns;

            for (int bypassingRow = 0; bypassingRow < columns - offset; bypassingRow++) {
                    pyramid[countColumns][setNumber] = inputNumbers.get(countNumber);
                    countNumber++;
                    setNumber += 2;
            }
        }
        return pyramid;
    }
}

