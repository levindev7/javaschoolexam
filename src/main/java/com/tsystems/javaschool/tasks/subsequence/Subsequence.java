package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();

        if (x.isEmpty()) {
            return true;
        }

        if (x.size() < y.size()) {
            int countX = 0;
            int countY = 0;
                while (y.size() > countY) {
                    if (x.get(countX).equals(y.get(countY))) {
                        countX++;
                        if (countX == x.size()) {
                            return true;
                        }
                    }
                    countY++;
                }

            return false;
        }
        return false;
    }
}
